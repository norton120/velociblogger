class CreateTableForThemes < ActiveRecord::Migration
  def change
    add_column :themes, :name, :string
    add_column :themes, :version, :float
    add_column :themes, :update_url, :string
  end
end
