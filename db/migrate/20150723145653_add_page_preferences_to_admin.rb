class AddPagePreferencesToAdmin < ActiveRecord::Migration
  def change
    add_column :blogs, :page_preferences, :string
  end
end
