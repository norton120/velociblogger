class BlogSocialToHash < ActiveRecord::Migration
  def change
  	remove_column :blogs, :twitter
  	remove_column :blogs, :facebook
  	remove_column :blogs, :linkedin  	
  end
end
