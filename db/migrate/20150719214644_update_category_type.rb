class UpdateCategoryType < ActiveRecord::Migration
  def change
  	change_column :posts, :category_id, 'integer USING CAST(category_id AS integer)'
  end
end
