applyTheme = function(){ 
 
  minBodyHeight();
  

  $(window).resize(function(){
    minBodyHeight();
  });



  //post#index//

  //format dates for post blocks  
  $('.posts_index h2').hide();

  $('#vb_post_cover_image, #vb_post_description').hide();

  $('#vb_sidebar').css({"border-top":"thick double #003366", "border-bottom" : "thick double #003366"});

  var post = $(".vb_post_block_date").children('span');

  $(post).each(function(){
  	var date = $(this).html().split(" ");
    
    var newDateSpan = "<div class='basic_post_block_date_day'>"+date[0]+"</div>"+
    "<div><span class='basic_post_block_date_month'>"+date[1]+"</span>"+
    "<span class='basic_post_block_date_year'>"+date[2]+"</span></div>";

    $(this).html(newDateSpan);	

  });
  
  
  $('#vb_header').css({
    'width':"110%", 
    "margin-left":"-1rem", 
    "margin-top":"-2rem", 
    "padding-top":"2rem", 
    "background-image":"radial-gradient(#D2E4F7, #efefef)",
    "background-position":"center", 
    "background-repeat":"repeat", 
    "background-size":"10px 10px"
  });

}

function minBodyHeight() 
{
  $('body').css("min-height", $(window).height() +"px");
}
