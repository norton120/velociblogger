class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :set_sidebar_vars, only: [:index, :show]
  
  def index
    @categories = Category.all
  end

  def show
    @posts = @category.posts.where('published = true').paginate(:page => params[:page], :per_page=>10)
  end

  def new
    @category = Category.new
    render :layout => 'admin'
  end

  def edit
    render :layout => 'admin'
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      redirect_to "/admin", notice: 'Category was successfully created.' 
    else
      flash.now[:errors] = "Unable to save category. " + @category.errors.full_messages.join(', ')
      render :new , :layout => 'admin'
    end
  end

  def update
    if @category.update(category_params)
      redirect_to @category, notice: 'Category was successfully updated.'
    else
      flash.now[:errors] = "Unable to save category. " + @category.errors.full_messages.join(', ')
      render :edit, :layout => 'admin'
    end
  end

  def destroy
    if Category.all.length < 1
      redirect_to "/admin", alert: 'At least one category must exist.' and return
    elsif @category.posts.length < 0
      redirect_to "/admin", alert: 'A category with posts cannot be removed.' and return
    else 
      @category.destroy
      redirect_to "/admin", notice: 'Category was successfully removed.'
    end    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find_by(:id=> params[:id]) || Category.find_by(:name => params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name, :stylesheet)
    end
  
end
