class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :login, only: [:edit, :update, :destroy, :new]
  before_action :cat_all, only: [:edit, :update, :create, :new]
  before_action :set_sidebar_vars, only: [:index, :show]
  

  def index
    params[:sort_by] = params[:sort_by] || "updated_at"
    order = params[:sort_by] == "title"? "ASC" : "DESC"
    @posts = Post.where('published = true').paginate(:page => params[:page], :per_page =>10).order("#{params[:sort_by]} #{order}")

  end

  def show
    @post.increment!(:views, 1)
  end

  def new
    @post = Post.new
    render :layout => 'admin'
    
  end

  def edit
    render :layout => 'admin'
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      redirect_to "/admin", notice: 'Post was successfully created.'
    else
      flash.now[:errors] = "Unable to save post. " + @post.errors.full_messages.join(', ')
      render :new, :layout => 'admin'
    end
  end

  def update
    if @post.update(post_params)
      redirect_to "/admin", notice: 'Post was successfully updated.' 
    else
      flash.now[:errors] = "Unable to save post. " + @post.errors.full_messages.join(', ')
      render :new, :layout => 'admin' 
    end
  end

  def destroy
    @post.destroy
    redirect_to "/admin", notice: 'Post was successfully destroyed.' 
  end

  private
    def set_post
      @post = Post.find(params[:id])
    end

    def post_params
      params.require(:post).permit(:title, :description, :body, :category_id, :published, :cover_image)
    end
    
    def cat_all
     @categories = Category.all
    end 

    
end
