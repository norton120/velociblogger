class ThemeController < ApplicationController
  before_action :login

  

  def upload
  
  render :layout=>'admin'
  end	

  def create
    if Theme.upload(params[:theme_zip].to_io) 
      redirect_to "/admin/settings", :notice=>"Theme successfully uploaded." and return
    else
      redirect_to :action=>"upload", :alert=>"Theme failed to upload." and return
    end
 
  end

end
