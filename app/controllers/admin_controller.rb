class AdminController < ApplicationController
  before_action :login
  before_action :posts_per_page

  def index
    sort_posts_by = params[:sort_posts_by] || "updated_at"
    order = to_bool(params[:desc])? "ASC" : "DESC"  
    
    if sort_posts_by == "category"
      @posts = Post.joins(:category).paginate(:page => params[:page], :per_page => @posts_per_page).order("categories.name #{order}")
    else  
      @posts = Post.paginate(:page => params[:page], :per_page => @posts_per_page).order("#{sort_posts_by} #{order}")
    end
    @categories = Category.all
 
  end

  def settings
    @themes =Theme.all
  end
  
  def update
    if @blog.update(admin_params)
      redirect_to "/admin/settings", :flash => { :notice => "Blog settings updated." } and return
    else
      redirect_to "/admin/settings", :flash => { :alert => "Blog settings failed to update." } and return
    end	
  end
  
  def admin_params
    params.require(:blog).permit(:title,:about,:theme, :social)
  end

  def posts_per_page
    cookies[:admin_posts_per_page] = params[:posts_per_page] || cookies[:admin_posts_per_page] || {value: '20', expires: 1.year.from_now}
    @posts_per_page = cookies[:admin_posts_per_page].to_i
  end

end
