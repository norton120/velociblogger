class AboutController < ApplicationController
  before_filter :login, :only=>[:edit, :update]
  before_action :set_sidebar_vars, only: [:index, :show]

  def index

  end

  def edit
  	@blog = Blog.all.first
  	render :layout => "admin"
  end
  
  
  def update
    if @blog.update(:about => params[:about])
      redirect_to "/admin", notice: 'About text was successfully updated.' 
    else
      flash.now[:errors] = "Unable to save about text. " + @blog.errors.full_messages.join(', ')
      render :new, :layout => 'admin' 
    end
  end	

end
