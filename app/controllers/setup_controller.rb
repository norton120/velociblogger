class SetupController < ApplicationController
  skip_before_filter :setup_required?
  before_filter :setup_complete?, :only=>[:step_1, :step_2, :step_3] 
  before_filter :setup_progress, :only=>[:step_2, :step_3, :step_4]
  layout false

  def step_1
    session[:setup_progress] = "step_2"

  end

  def step_2
    @google_client_id = params[:client_id]
    @google_client_secret = params[:client_secret]
    @google_email_address = params[:email_address]
  end

  def step_3
    setup = Setup.new(params[:setup])
    unless setup.generate_setup_file
      redirect_to :action=>"step_1", :alert=>"Something went wrong. Please try again." and return
    end

    unless Theme.upload(File.new("#{Rails.root}/themes/basic.zip"))
      redirect_to :action=>"step_1", :alert=>"Something went wrong. Please try again." and return
    end

  end

  def step_4
    Blog.delete_all
    Blog.create(:title=>params[:title], :theme=>"basic") && Category.create(:name=>params[:category])
  end

  private 

  def setup_complete?
    if File.exist?("#{Rails.root}/config/initializers/velociblogger_setup.rb")
      raise not_found
    end  
  end	

  def setup_progress
    if session[:setup_progress].to_s != action_name
      redirect_to "/" and return
    end
    step = (action_name.last.to_i+1).to_s
    next_action = action_name[0...-1] + step
    session[:setup_progress] = next_action

  end

end
