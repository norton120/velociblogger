class Category < ActiveRecord::Base
  has_many :posts
  before_validation :downcase_name
  validates_presence_of :name
  validates_uniqueness_of :name

  def downcase_name
    self.name.downcase!
  end

end
