class Post < ActiveRecord::Base
	belongs_to :category
	validates_presence_of :title, :description, :body
    validates_inclusion_of :published, :in => [true, false]
	validates_uniqueness_of :title

def vanity_url
  "/blog/#{self.id}/"+Rack::Utils.escape(self.title)
end



end
