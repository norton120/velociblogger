class Theme < ActiveRecord::Base

  def self.upload(theme_file)

    require 'zip'

    Zip::File.open(theme_file) do |zip_file|
     
      manifest = YAML.load(zip_file.glob('*/manifest.yml').first.get_input_stream.read).symbolize_keys 
      %w(name version update_url).each do |param|
        raise ArgumentError.new("Manifest.yml is missing required key #{param}.") unless manifest.has_key?(param.to_sym) 
      end 

      name = manifest[:name] 
   
      theme_dir = "#{Rails.root}/public/themes/#{manifest[:name]}"
      
      FileUtils.remove_dir(theme_dir, true)
      
      zip_file.each do |file|
        f_name = file.name.remove(file.name.split("/")[0])
        file_path = File.join(theme_dir, f_name)
        FileUtils.mkdir_p(File.dirname(file_path))
        zip_file.extract(file, file_path)
      end
      
      theme = Theme.find_or_initialize_by(name: name)
      theme.version = manifest[:version] 
      theme.update_url = manifest[:update_url] 
      theme.save 
    end  
  end  
  


end
