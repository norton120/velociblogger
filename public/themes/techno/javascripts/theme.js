applyTheme = function(){ 

  $('nav a[href="/categories"], .categories_index h2').html('topics');

  //posts
  
  function stylePostBlockDates(){
    $('.vb_post_block_date').each(function(){
    	var cat = $(this).closest('.vb_post_block').find('.vb_post_block_category');
    	var dateEl = $(this).children('span');
        var date = dateEl.html().toString().split(" ");
  	    dateEl.html(date[0]+" "+date[1]+(date[2]!= new Date().getFullYear()? date[2]:""));
  	    $(cat).after("<div class='vb_post_block_date'>"+date[0]+" "+date[1]+(date[2]!= new Date().getFullYear()? date[2]:"")+"</div>");
    	$(this).remove();

    });
    
  	


  };


  function hideDescriptions(){
  	$('#vb_post_description').hide();
  };

  hideDescriptions();
  stylePostBlockDates();
};
