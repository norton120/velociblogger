ckInjector = function(target)
{
  var ckeditor_distro;
  if(mobile_p || mobile_l)
  {
    ckeditor_distro = "basic";
  }
  else if(tablet_p || tablet_l)
  {
    ckeditor_distro = "standard";
  }
  else
  {
    ckeditor_distro = "full-all"
  }
  
  $.getScript("//cdn.ckeditor.com/4.5.1/"+ckeditor_distro+"/ckeditor.js", function(){
    CKEDITOR.plugins.addExternal( 'pastebase64', '/vendor/javascripts/ckeditor_plugins/pastebase64.js', '' );
    CKEDITOR.replace( target, {extraPlugins:'pastebase64', resize_enabled:false, width: "100%", height:"25rem"});  
    CKEDITOR.config.format_tags = 'p;h3;h4;h5;h6;pre;address;div';  
    $('body').append("<style>#cke_textEditor{margin:auto;} .cke_source{background:black!important;color:lightGreen!important;padding:1rem!important;}</style>");

  });

}

