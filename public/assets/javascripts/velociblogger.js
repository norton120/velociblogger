var mobile_p;
var mobile_l;
var tablet_p;
var tablet_l;
var desktop;

function setDevice()
{
  var w = $(window).width();
  mobile_p = w < 480;
  mobile_l = w >= 480 && w < 768;
  tablet_p = w >= 768 && w < 1024;
  tablet_l = w >= 1024 && w < 1440;
  desktop = w >=1440;
}


setDevice();

$(window).resize(function(){
  minBodyHeight();

});

applyTheme = function(){ return true; } 
